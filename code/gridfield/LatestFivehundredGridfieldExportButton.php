<?php
class LatestFivehundredGridfieldExportButton extends GridFieldExportButton {

    /**
     * Place the export button in a <p> tag below the field
     */
    public function getHTMLFragments($gridField) {
        $button = new GridField_FormAction(
            $gridField,
            'export',
            _t('TableListField.CSVEXPORT', 'Export to CSV'),
            'export',
            null
        );
        $button->setAttribute('data-icon', 'download-csv');
        $button->addExtraClass('no-ajax action_export');
        $button->setForm($gridField->getForm());
        return array(
            $this->targetFragment => '<p class="grid-csv-button">' . $button->Field() . '</p><span>Note: only the newest 500 entries will be exported.</span>',
        );
    }

    public function generateExportFileData($gridField) {
        $separator = $this->getCsvSeparator();
        $csvColumns = $this->getExportColumnsForGridField($gridField);
        $fileData = array();

        if($this->csvHasHeader) {
            $headers = array();

            // determine the CSV headers. If a field is callable (e.g. anonymous function) then use the
            // source name as the header instead

            foreach($csvColumns as $columnSource => $columnHeader) {
                if (is_array($columnHeader) && array_key_exists('title', $columnHeader)) {
                    $headers[] = $columnHeader['title'];
                } else {
                    $headers[] = (!is_string($columnHeader) && is_callable($columnHeader)) ? $columnSource : $columnHeader;
                }
            }

            $fileData[] = $headers;
        }

        //Remove GridFieldPaginator as we're going to export the entire list.
        $gridField->getConfig()->removeComponentsByType('GridFieldPaginator');

        $items = $gridField->getManipulatedList();

        // @todo should GridFieldComponents change behaviour based on whether others are available in the config?
        foreach($gridField->getConfig()->getComponents() as $component){
            if($component instanceof GridFieldFilterHeader || $component instanceof GridFieldSortableHeader) {
                $items = $component->getManipulatedData($gridField, $items);
            }
        }

        foreach($items->limit(null) as $item) {
            if(!$item->hasMethod('canView') || $item->canView()) {
                $columnData = array();

                foreach($csvColumns as $columnSource => $columnHeader) {
                    if(!is_string($columnHeader) && is_callable($columnHeader)) {
                        if($item->hasMethod($columnSource)) {
                            $relObj = $item->{$columnSource}();
                        } else {
                            $relObj = $item->relObject($columnSource);
                        }

                        $value = $columnHeader($relObj);
                    } else {
                        $value = $gridField->getDataFieldValue($item, $columnSource);

                        if($value === null) {
                            $value = $gridField->getDataFieldValue($item, $columnHeader);
                        }
                    }

                    $value = str_replace(array("\r", "\n"), "\n", $value);

                    // [SS-2017-007] Sanitise XLS executable column values with a leading tab
                    if (!Config::inst()->get(get_class($this), 'xls_export_disabled')
                        && preg_match('/^[-@=+].*/', $value)
                    ) {
                        $value = "\t" . $value;
                    }
                    $columnData[] = $value;
                }

                $fileData[] = $columnData;
            }

            if($item->hasMethod('destroy')) {
                $item->destroy();
            }

            if (count($fileData) >= 500) {
                break;
            }
        }

        // Convert the $fileData array into csv by capturing fputcsv's output
        $csv = fopen('php://temp', 'r+');
        foreach($fileData as $line) {
            fputcsv($csv, $line, $separator);
        }
        rewind($csv);
        return stream_get_contents($csv);
    }
}