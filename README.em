#Extend you index for solar server
#depends on module "fulltextsearch" https://github.com/silverstripe-labs/silverstripe-fulltextsearch.git

<?php
class MyIndex extends SolrIndexExtension
{
    function init()
    {
        // Page class is already added in parent, and content and title
        parent::init();

        //add non-standard fields if needed:
        //$this->addFulltextField('BoxRedTitle');
    }
}

#include the Javascript on yout home page

Requirements::javascript('typeahead/javascript/bootstrap-typeahead.min.js');

# configuration in _config.php - just copy below, no adjustments needed
Solr::configure_server(array(
    'host' => '218.185.224.227', // this is our hothouse solr server
    'indexstore' => array(
        'mode' => 'webdav',
        'path' => '/soltedit',
        'remotepath' => '',
    ),
	'extraspath' => BASE_PATH . '/silverstripe-typeahead/solr/extras',
	'templatespath' => BASE_PATH . '/silverstripe-typeahead/solr/templates'
));

# Enable some filed to be typeahead
#html

<input id="searchform" type="text" class="form-control" placeholder="Search..." autocomplete="off" />

#javascript

    $('#searchform').typeahead({
        ajax: {
            url: "/home/SolrSuggest",
            method: 'post',
            triggerLength: 1
        },
        onSelect: displayResult
    });
    function displayResult(item) {
        $('.alert').show().html('You selected <strong>' + item.value + '</strong>: <strong>' + item.text + '</strong>');
    }


#Flush your cache and rebuid your system, visit this web
http://localhost/dev/build?flush=1


