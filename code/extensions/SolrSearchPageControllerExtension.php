<?php

/**
 * User: Ivan Muller
 * Mail: mullerivan@gmail.com
 * web mullerivan.com.ar
 * Date: 22/01/14
 * Time: 3:20 PM
 */
class SolrSearchPageControllerExtension extends DataExtension {

	public function onAfterInit() {
		if($SolrSearchPage = SolrSearchPage::get()->first()) {
			$Link = $SolrSearchPage->Link('suggest');
			Requirements::customScript(
<<<JS
//if(typeof typeahead != 'undefined') {
	$('.search-form input').typeahead({
		ajax: {
			url: "$Link",
			method: 'get',
			triggerLength: 1
		},
		onSelect: function() {
		}
	});
//}
JS
			);
		}
	}

	public function SolrSearchForm() {
		$SolrSearchPage = SolrSearchPage::get()->first();
		if($SolrSearchPage && $Controller = new SolrSearchPage_Controller($SolrSearchPage)) {
			return $Controller->SolrTypeAheadForm()->addExtraClass('navbar-form search-form');
		}
	}

}