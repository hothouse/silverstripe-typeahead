<?php
class FullTextSearchPhrase extends DataObject {
    private static $db = array(
        'SearchPhrase' => 'Varchar(255)'
    );

    private static $summary_fields = array(
        'Created', 'SearchPhraseLink'
    );

    private static $export_columns = array(
        'Created' => 'Created',
        'SearchPhrase' => 'Search Phrase',
        'SearchPhraseUrl' => 'URL'
    );

    private static $default_sort = array(
        'Created DESC'
    );

    public static function get_gridfieldconfig($Sortable = true, $SortColumn = 'SortID') {
        $gridFieldConfig = GridFieldConfig::create()->addComponents(
            new GridFieldToolbarHeader(),
            new GridFieldSortableHeader(),
            $cols = new GridFieldDataColumns(),
            new GridFieldPaginator(50),
            $export = new LatestFivehundredGridfieldExportButton()
        );

        $cols->setFieldCasting(array(
            'SearchPhraseLink' => 'HTMLVarchar->RAW'
        ));

        $export->setExportColumns(self::$export_columns);

        return $gridFieldConfig;
    }

    public function SearchPhraseLink() {
        $searchPage = SolrSearchPage::get()->first();
        return sprintf('<a href="%s" target="_blank">%s</a>', $this->SearchPhraseUrl(), $this->SearchPhrase);
    }

    public function SearchPhraseUrl() {
        $searchPage = SolrSearchPage::get()->first();
        $searchHandler = $searchPage->SearchResultsHandler ? $searchPage->SearchResultsHandler : 'results';
        return Controller::join_links($searchPage->AbsoluteLink(), '/' . $searchHandler, '?Search=' . $this->SearchPhrase);
    }

    public static function save($phrase) {
        $obj = new FullTextSearchPhrase();
        $obj->SearchPhrase = $phrase;
        $obj->write();
    }
}