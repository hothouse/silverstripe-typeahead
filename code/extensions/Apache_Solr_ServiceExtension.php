<?php

/**
 * User: Ivan Muller
 * Mail: mullerivan@gmail.com
 * web mullerivan.com.ar
 * Date: 22/01/14
 * Time: 10:00 AM
 */
class Apache_Solr_ServiceExtension extends Apache_Solr_Service {
	const SUGGEST_SERVLET = 'suggest';

	public function suggest($query, $offset = 0, $limit = 10, $params = array(), $method = self::METHOD_GET) {
		$_suggestUrl = $this->_constructUrl(self::SUGGEST_SERVLET);
		// ensure params is an array
		if (!is_null($params)) {
			if (!is_array($params)) {
				// params was specified but was not an array - invalid
				throw new Apache_Solr_InvalidArgumentException("\$params must be a valid array or null");
			}
		} else {
			$params = array();
		}

		// construct our full parameters

		// common parameters in this interface
		$params['wt'] = self::SOLR_WRITER;
		$params['json.nl'] = $this->_namedListTreatment;

		$params['q'] = $query;
		$params['start'] = $offset;
		$params['rows'] = $limit;

		$queryString = $this->_generateQueryString($params);

		if ($method == self::METHOD_GET) {
			return $this->_sendRawGet($_suggestUrl.$this->_queryDelimiter.$queryString);
		} else {
			if ($method == self::METHOD_POST) {
				return $this->_sendRawPost(
					$_suggestUrl,
					$queryString,
					false,
					'application/x-www-form-urlencoded; charset=UTF-8'
				);
			} else {
				throw new Apache_Solr_InvalidArgumentException("Unsupported method '$method', please use the Apache_Solr_Service::METHOD_* constants");
			}
		}
	}
}