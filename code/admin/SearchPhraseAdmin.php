<?php
class SearchPhraseAdmin extends ModelAdmin {
    private static $managed_models = array('FullTextSearchPhrase');
    private static $url_segment = 'search_phrases';
    private static $menu_title = 'Search Phrases';

    public function getEditForm($id = null, $fields = null) {
        $form = parent::getEditForm($id = null, $fields = null);
        $modelClass = $this->modelClass;
        if (method_exists($modelClass, 'get_gridfieldconfig')) {
            $form
                ->Fields()
                ->fieldByName($this->sanitiseClassName($this->modelClass))
                ->setConfig($modelClass::get_gridfieldconfig());
        }

        return $form;
    }
}