<?php
/**
 * User: Ivan Muller
 * Mail: mullerivan@gmail.com
 * web mullerivan.com.ar
 * Date: 22/01/14
 * Time: 2:53 PM
 */

abstract class SolrIndexExtension extends SolrIndex {

	public function init() {
		$this->addClass('Page');
		$this->addFulltextField('Title');
		$this->addFulltextField('Content');
		$this->addFulltextField('MetaDescription');
	}

    public function suggest(SearchQuery $query, $offset = -1, $limit = -1, $params = array()) {
        $options = Solr::solr_options();
        $service = new Apache_Solr_ServiceExtension($options['host'],$options['port'],$options['path'].'/'.get_class($this));
        SearchVariant::with(count($query->classes) == 1 ? $query->classes[0]['class'] : null)->call('alterQuery', $query, $this);
        $q = array();
        $fq = array();
        // Build the search itself
        $text = '';
        foreach ($query->search as $search) {
            $text = $search['text'];
            preg_match_all('/"[^"]*"|\S+/', $text, $parts);

            $fuzzy = $search['fuzzy'] ? '~' : '';

            foreach ($parts[0] as $part) {
                $fields = (isset($search['fields'])) ? $search['fields'] : array();
                if(isset($search['boost'])) $fields = array_merge($fields, array_keys($search['boost']));
                if ($fields) {
                    $searchq = array();
                    foreach ($fields as $field) {
                        $boost = (isset($search['boost'][$field])) ? '^' . $search['boost'][$field] : '';
                        $searchq[] = "{$field}:".$part.$fuzzy.$boost;
                    }
                    $q[] = '+('.implode(' OR ', $searchq).')';
                }
                else {
                    $q[] = '+'.$part.$fuzzy;
                }
            }
        }

        // Filter by class if requested

        $classq = array();

        foreach ($query->classes as $class) {
            if (!empty($class['includeSubclasses'])) $classq[] = 'ClassHierarchy:'.$class['class'];
            else $classq[] = 'ClassName:'.$class['class'];
        }

        if ($classq) $fq[] = '+('.implode(' ', $classq).')';

        // Filter by filters

        foreach ($query->require as $field => $values) {
            $requireq = array();

            foreach ($values as $value) {
                if ($value === SearchQuery::$missing) {
                    $requireq[] = "(*:* -{$field}:[* TO *])";
                }
                else if ($value === SearchQuery::$present) {
                    $requireq[] = "{$field}:[* TO *]";
                }
                else if ($value instanceof SearchQuery_Range) {
                    $start = $value->start; if ($start === null) $start = '*';
                    $end = $value->end; if ($end === null) $end = '*';
                    $requireq[] = "$field:[$start TO $end]";
                }
                else {
                    $requireq[] = $field.':"'.$value.'"';
                }
            }

            $fq[] = '+('.implode(' ', $requireq).')';
        }

        foreach ($query->exclude as $field => $values) {
            $excludeq = array();
            $missing = false;

            foreach ($values as $value) {
                if ($value === SearchQuery::$missing) {
                    $missing = true;
                }
                else if ($value === SearchQuery::$present) {
                    $excludeq[] = "{$field}:[* TO *]";
                }
                else if ($value instanceof SearchQuery_Range) {
                    $start = $value->start; if ($start === null) $start = '*';
                    $end = $value->end; if ($end === null) $end = '*';
                    $excludeq[] = "$field:[$start TO $end]";
                }
                else {
                    $excludeq[] = $field.':"'.$value.'"';
                }
            }

            $fq[] = ($missing ? "+{$field}:[* TO *] " : '') . '-('.implode(' ', $excludeq).')';
        }

        if(!headers_sent()) {
            if ($q) header('X-Query: '.implode(' ', $q));
            if ($fq) header('X-Filters: "'.implode('", "', $fq).'"');
        }

        if ($offset == -1) $offset = $query->start;
        if ($limit == -1) $limit = $query->limit;
        if ($limit == -1) $limit = SearchQuery::$default_page_size;

        $params = array_merge($params, array('fq' => implode(' ', $fq)));

        $res = $service->suggest(
            $q ? implode(' ', $q) : '*:*',
            $offset,
            $limit,
            $params,
            Apache_Solr_Service::METHOD_GET
        );
        if($res->getHttpStatus() >= 200 && $res->getHttpStatus() < 300) {
            $json_response = json_decode($res->getRawResponse(),true);
            $response = array();
			if (isset($json_response['spellcheck']['suggestions'][$text]['suggestion']) && count($json_response['spellcheck']['suggestions'][$text]['suggestion'])) {
				foreach ($json_response['spellcheck']['suggestions'][$text]['suggestion'] as $key => $suggestion) {
					$id =(string)$key;
					$response[] = array('id'=> $id,'name'=>$suggestion);
				}
			}
            return $response;
        } else {
            return  'HTTP ERROR';
        }
    }

	function getFieldDefinitions() {
		$xml = array();
		$stored = Director::isDev() ? "stored='true'" : "stored='false'";

		$xml[] = "";

		// Add the hardcoded field definitions

		$xml[] = "<field name='_documentid' type='string' indexed='true' stored='true' required='true' />";

		$xml[] = "<field name='ID' type='tint' indexed='true' stored='true' required='true' />";
		$xml[] = "<field name='ClassName' type='string' indexed='true' stored='true' required='true' />";
		$xml[] = "<field name='ClassHierarchy' type='string' indexed='true' stored='true' required='true' multiValued='true' />";

		// Add the fulltext collation field

		$xml[] = "<field name='_text' type='htmltext' indexed='true' $stored multiValued='true' />" ;
		$xml[] = "<field name='auto_suggest' type='word_autocomplete' indexed='true' $stored multiValued='true' />";

		// Add the user-specified fields

		foreach ($this->fulltextFields as $name => $field) {
			$xml[] = $this->getFieldDefinition($name, $field, self::$fulltextTypeMap);
		}

		foreach ($this->filterFields as $name => $field) {
			if ($field['fullfield'] == 'ID' || $field['fullfield'] == 'ClassName') continue;
			$xml[] = $this->getFieldDefinition($name, $field);
		}

		foreach ($this->sortFields as $name => $field) {
			if ($field['fullfield'] == 'ID' || $field['fullfield'] == 'ClassName') continue;
			$xml[] = $this->getFieldDefinition($name, $field);
		}

		return implode("\n\t\t", $xml);
	}

	function getCopyFieldDefinitions() {
		$xml = array();

		foreach ($this->fulltextFields as $name => $field) {
			$xml[] = "<copyField source='{$name}' dest='_text' />";
			$xml[] = "<copyField source='{$name}' dest='auto_suggest' />";
		}

		foreach ($this->copyFields as $source => $fields) {
			foreach($fields as $fieldAttrs) {
				$xml[] = $this->toXmlTag('copyField', $fieldAttrs);
			}
		}

		return implode("\n\t", $xml);
	}
}